import configparser
import os

from colorama import Fore, init

from fabric import Connection, task


init()


def blue(text, bold=False):
    return Fore.BLUE + text + Fore.WHITE


def green(text, bold=False):
    return Fore.GREEN + text + Fore.WHITE


def red(text, bold=False):
    return Fore.RED + text + Fore.WHITE


def yellow(text, bold=False):
    return Fore.YELLOW + text + Fore.WHITE


def _print(output):
    print(output)


def print_command(command, conn):
    _print(
        blue("{}@{}$ ".format(conn.user, conn.host), bold=True)
        + yellow(command, bold=True)
        + red(" ->", bold=True)
    )


# Función para crear conexiones a los hosts
def connect(ctx):
    connections = []
    if not hasattr(ctx, "host"):
        config = configparser.ConfigParser()
        config.read("hosts.conf", encoding="utf-8-sig")

        for section in config.sections():
            vals = {"host": config[section]["host"], "user": config[section]["user"]}
            if config[section].get("key_filename", False):
                vals.update(
                    {
                        "connect_kwargs": {
                            "key_filename": os.path.expanduser(
                                config[section]["key_filename"]
                            )
                        }
                    }
                )
            if config[section].get("password"):
                vals.update(
                    {"connect_kwargs": {"password": config[section]["password"]}}
                )

            connections.append(Connection(**vals))
    else:
        connections = [ctx]
    return connections


@task
def run(ctx, command, show=True):
    """
    Runs a shell command on the remote server.
    """
    connections = connect(ctx)
    for conn in connections:
        if show:
            print_command(command, conn)
        conn.run(command, hide=(not show), shell="/bin/sh")


@task
def ping(ctx):
    run(ctx, "ping -c 3 www.google.com")


@task
def uname(ctx):
    run(ctx, "uname -a")
